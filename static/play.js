/*
P5Js canvas and scripts for rendering the pixels of a particular GridPlace game.

Author: Delta
*/


//Game parameters
var game_pixels = []
var game_board = []
var game_address
var game_width = 0
var game_height = 0
var pix_selected = false
var SCALE = 10


//Document element variables
var pixel_info = null
var play_info = null
var x_in = null
var y_in = null
var col_in = null


function preload() {
  let url = document.URL.replace('/play', '/getpixels')
  game_board = loadJSON(url)
  game_address = url.substring(url.indexOf('=')+1)
}


function setup() {
  game_board = game_board['grid']
  if (game_board) {
    game_width = game_board.length
    game_height = game_board[0].length
  } else {
    pixel_info.innerHTML = 'FAILED TO GET PIXEL DATA'
    return
  }
  createCanvas(game_width * SCALE, game_height * SCALE)

  //Creating required page elements
  pixel_info = document.getElementById('pixel-info')
  play_info = document.getElementById('play-info')
  x_in = document.getElementById('x-input')
  y_in = document.getElementById('y-input')
  col_in = document.getElementById('colour-input')

  noStroke()
  for (let x = 0; x < game_width; x++) {
    for (let y = 0; y < game_height; y++) {
      fill(color( game_board[x][y]['colour'][0],
                  game_board[x][y]['colour'][1],
                  game_board[x][y]['colour'][2],
                  255
                ))
      rect(x*SCALE, y*SCALE, SCALE, SCALE)
    }
  }
}


function mouseClicked() {
  let outside_sidebar = is_mouse_outside_sidebar(mouseX, mouseY)
  let on_canvas = check_game_index(mouseX, mouseY)
  if (outside_sidebar && on_canvas) {
    pix_selected = true
    change_pixel_info(mouseX, mouseY)
    fill_input(mouseX, mouseY)
  } else if (outside_sidebar && !on_canvas) {
    pix_selected = false
    pixel_info.innerHTML = 'Mouse-over a pixel on the canvas to show its data.'
  }
}


function mouseMoved() {
  if (is_mouse_outside_sidebar(mouseX, mouseY) && !pix_selected && check_game_index(mouseX, mouseY)) {
    change_pixel_info(mouseX, mouseY)
  }
}


function change_pixel_info(mx, my) {
  let x = floor(mx/SCALE)
  let y = floor(my/SCALE)
  let pix = game_board[x][y]
  if (pix['time'] > 0) {
    let t_obj = new Date(pix['time'] * 1000)
    let sec = t_obj.getSeconds() < 10 ? '0' + t_obj.getSeconds() : t_obj.getSeconds()
    let min = t_obj.getMinutes() < 10 ? '0' + t_obj.getMinutes() : t_obj.getMinutes()
    let hour = t_obj.getHours()
    let day = t_obj.getDate()
    let month = t_obj.getMonth() + 1
    let year = t_obj.getFullYear()
    let timestamp = `${day}-${month}-${year} ${hour}:${min}:${sec}`

    pixel_info.innerHTML = `
      Pixel at (${x}, ${y}) placed by <span class="data-text">${pix['editor']}</span><br>
      at ${timestamp}<br>
      with TXID <span class="data-text">${pix['txid']}</span><br>
      This pixel costs <span class="data-text">${pix['price']}</span>
    `
  } else {
    pixel_info.innerHTML = `Pixel at (${x}, ${y}) has not been placed yet.`
  }
}


function get_colours_from_pixel_json(json) {
  let final_array = []
  for (let y = 0; y < json.length; y++) {
    for (let x = 0; x < json[0].length; x++) {
      final_array.push(
        json[x][y]['colour'][0],
        json[x][y]['colour'][1],
        json[x][y]['colour'][2],
        255
      )
    }
  }
  return final_array
}


function check_game_index(x, y) {
  if (x == "" || y == "") {
    return false
  }
  return (x >= 0 && x < game_width * SCALE && y >= 0 && y < game_height * SCALE)
}


function is_mouse_outside_sidebar(x, y) {
  let in_sidebar = x-window.scrollX > (windowWidth-420)
  return open && !in_sidebar || !open
}


function fill_input(x, y) {
  document.getElementById('x-input').value = floor(x/SCALE)
  document.getElementById('y-input').value = floor(y/SCALE)
}


function process_form() {
  play_info.innerHTML = 'Loading...'
  let x = x_in.value
  let y = y_in.value
  let col = col_in.value.replace('#', '%23')
  let token = get_cookie('gridplace-token')
  if (check_game_index(x, y)) {
    let url = document.URL.replace('/play', '/place') +
    `&x=${x}&y=${y}&col=${col}&token=${token}`
    loadJSON(url, (tx_info) => {play_info.innerHTML = tx_info['message'];})
  } else {
    play_info.innerHTML = 'Invalid data entered.'
  }
}


//Code from w3schools
function get_cookie (cname) {
  var name = cname + '='
  var decodedCookie = decodeURIComponent(document.cookie)
  var ca = decodedCookie.split(';')
  for (var i = 0; i <ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ''
}
