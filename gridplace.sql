CREATE DATABASE IF NOT EXISTS gridplace;

USE gridplace;


CREATE TABLE IF NOT EXISTS udb (
  uname CHAR(18),
  pwhash BLOB,
  balance DOUBLE,
  address CHAR(34),
  last_place BIGINT UNSIGNED,

  PRIMARY KEY (uname)
);

CREATE TABLE IF NOT EXISTS tokens (
  tk BIGINT UNSIGNED,
  uname CHAR(18),
  expiry BIGINT UNSIGNED,

  PRIMARY KEY (tk)
);

CREATE TABLE IF NOT EXISTS transactions (
  txid CHAR(64),
  uname CHAR(18),
  amount DOUBLE,

  PRIMARY KEY (txid, uname)
);
