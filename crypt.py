import scrypt
from base58 import b58encode_int as base58e
from os import urandom


MAX_TIME = 0.125
UID_LEN = 18


def hash_pw(password):
    return scrypt.encrypt(str(urandom(64)), password, maxtime=MAX_TIME)


def check_pw(guess: str, dbpass: bytes):
    try:
        scrypt.decrypt(dbpass, guess, maxtime=MAX_TIME)
        return True
    except scrypt.error:
        return False


def get_rand_token():
    uid = ''
    while len(uid) < UID_LEN:
        uid = str(int.from_bytes(urandom(UID_LEN), 'big'))
    return uid[:UID_LEN]
