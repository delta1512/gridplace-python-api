'''
The main web API file that interfaces with the protocol to produce an
interactive webpage. The API is based on python's FLASK API server.

Dependencies:
- Flask

Author: Delta
'''

from os import path
import re
from time import time
import requests

from flask import Flask, request, redirect, render_template, make_response, url_for, jsonify, g
from protocol import gridplace, blockchain
w = blockchain.wallet
import secret as s
import queries as q
import crypt


### Game and app variables
app = Flask(__name__)
game = gridplace.GridPlace()
###


### CONSTANTS
HOST = 'https://gridplace.duckdns.org/'
TOKEN_EXP = 3 * 31 * 24 * 60 * 60 #3 months
RECAPTCHA_URL = 'https://www.google.com/recaptcha/api/siteverify'
FAIL_PLACE = {'message' : 'Failed to place pixel, check values and try again.'}
NO_LOGIN = {'message' : 'You are not logged in, log in <a href="{}login">here</a>'.format(HOST)}
WAIT = {'message' : 'Please wait for your previous transaction to be confirmed. (3min max)'}
INSUFFICIENT_BAL = {'message' : 'You don\'t have enough GRC to make that ransfer.'}
TX_SUCCESS_MSG = 'Transaction successful! Wait a few minutes and refresh the page. Check transaction status <a target="_blank" href="https://gridcoinstats.eu/tx/{}">here</a>'
ROBOTSTXT = 'User-agent: *\nAllow: /home'
###


### Regex checks
def valid_username(uname):
    return bool(re.fullmatch('[0-9a-zA-Z_\.\-@#]{3,18}', uname))


def valid_colour(col):
    return bool(re.fullmatch('#[0-9a-fA-F]{6}', col))
###


### FUNCTIONS
#Closes the database when the API closes
@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()


def get_account_page(token):
    account_dict = q.get_account_from_token(token)
    return render_template('account.html', acc=account_dict, r=round)


def verify_recaptcha(user_resp):
    if user_resp is None:
        return False

    r = requests.post(RECAPTCHA_URL,
        data={  'secret'    :   s.RECAPTCHA_SECRET,
                'response'  :   user_resp})
    if r.status_code == 200:
        if r.json().get('score', 0) > 0.3:
            return True
    return False


def make_place_tx(current_game, x, y, col, tk):
    user_account = q.get_account_from_token(tk)
    username = user_account['username']
    price = current_game.cells[x][y].get_price()
    if not q.can_place(username):
        return WAIT
    if q.can_afford(price + s.FEE, username):
        txid = w.send_tx(current_game.game_address, price, comment='{} {} {}'.format(x, y, col))
        if not type(txid) is int:
            q.spend(price + s.FEE, username, txid)
            return {'message' : TX_SUCCESS_MSG.format(txid), 'success' : True}
        return FAIL_PLACE
    else:
        return INSUFFICIENT_BAL
###


### FRONT-END API
@app.route('/robots.txt', methods=['GET'])
def robots_txt():
    return ROBOTSTXT

@app.route('/', methods=['GET'])
@app.route('/home', methods=['GET'])
def home():
    return render_template('index.html')


@app.route('/games', methods=['GET'])
def games():
    for game_address in game.grids:
        pic_path = 'static/img/{}.png'.format(game_address)
        if game.search() or (not path.exists(pic_path)):
            game.gen_grid_image(game_address, pic_path)
    return render_template('games.html', current_game=game, enum=enumerate)


@app.route('/play', methods=['GET'])
def play():
    game.search()
    game_address = request.args.get('addr', None)
    if game_address in game.grids:
        return render_template('play.html')
    else:
        return redirect(url_for('games'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == 'POST':
        username = request.form.get('username', '')
        password = request.form.get('password', '')
        token = int(request.cookies.get('gridplace-token', 0))
        if not all([valid_username(username), password]):
            return render_template('login.html', failed=True, RECAPTCHA=s.CLIENT_TOK)

        if q.valid_token(token):
            return get_account_page(token)
        if not verify_recaptcha(request.form.get('g-recaptcha-response', None)):
            return render_template('login.html', failed=True, RECAPTCHA=s.CLIENT_TOK)
        if not q.user_exists(username):
            return render_template('login.html', failed=True, RECAPTCHA=s.CLIENT_TOK)
        if not q.password_correct(username, password):
            return render_template('login.html', failed=True, RECAPTCHA=s.CLIENT_TOK)

        new_token = crypt.get_rand_token()
        q.set_user_token(new_token, username, int(time()+TOKEN_EXP))
        resp = make_response(get_account_page(new_token))
        resp.set_cookie('gridplace-token', new_token.encode(), expires=int(time()+TOKEN_EXP))

        return resp
    else:
        return render_template('login.html', failed=False, RECAPTCHA=s.CLIENT_TOK)


@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form.get('username', '')
        password = request.form.get('password', '')
        password_confirm = request.form.get('password-confirm', '')
        if not all([username, password, password_confirm]):
            return render_template('register.html', failed='Please fill all fields.', RECAPTCHA=s.CLIENT_TOK)

        if not verify_recaptcha(request.form.get('g-recaptcha-response', None)):
            return render_template('register.html', failed='RECAPTCHA failed, try again.', RECAPTCHA=s.CLIENT_TOK)
        if not valid_username(username):
            return render_template('register.html', failed='Username is invalid', RECAPTCHA=s.CLIENT_TOK)
        if len(password) < 7:
            return render_template('register.html', failed='Password too short.', RECAPTCHA=s.CLIENT_TOK)
        if len(password) > 2048:
            return render_template('register.html', failed='Password too long.', RECAPTCHA=s.CLIENT_TOK)
        if password != password_confirm:
            return render_template('register.html', failed='Passwords do not match.', RECAPTCHA=s.CLIENT_TOK)
        if q.user_exists(username):
            return render_template('register.html', failed='Username already exists.', RECAPTCHA=s.CLIENT_TOK)

        q.new_user(username, crypt.hash_pw(password))

        new_token = crypt.get_rand_token()
        q.set_user_token(new_token, username, int(time()+TOKEN_EXP))
        resp = make_response(get_account_page(new_token))
        resp.set_cookie('gridplace-token', new_token.encode(), expires=int(time()+TOKEN_EXP))

        return resp
    else:
        return render_template('register.html', failed=False, RECAPTCHA=s.CLIENT_TOK)


@app.route('/account', methods=['GET'])
def account():
    token = int(request.cookies.get('gridplace-token', 0))
    if q.valid_token(token):
        return get_account_page(token)
    return redirect(url_for('login'))
###


### BACK-END API
@app.route('/place', methods=['GET'])
def place():
    try:
        x_pos = int(request.args.get('x', 0))
        y_pos = int(request.args.get('y', 0))
    except ValueError:
        return jsonify(FAIL_PLACE)
    colour = request.args.get('col', None)
    token = request.args.get('token', '')
    game_address = request.args.get('addr', None)

    if not game_address in game.grids:
        return jsonify(FAIL_PLACE)
    current_game = game.grids[game_address]

    if not 0 <= x_pos < current_game.width:
        return jsonify(FAIL_PLACE)

    if not 0 <= y_pos < current_game.height:
        return jsonify(FAIL_PLACE)

    if not valid_colour(colour):
        return jsonify(FAIL_PLACE)

    if not q.valid_token(token):
        return jsonify(NO_LOGIN)

    return jsonify(make_place_tx(current_game, x_pos, y_pos, colour, token))


@app.route('/getpixels', methods=['GET'])
def get_pixels():
    game_address = request.args.get('addr', None)
    if game_address in game.grids:
        return jsonify(game.grids[game_address].jsonify())
    else:
        return redirect(url_for('games'))


@app.route('/getgrids', methods=['GET'])
def get_grids():
    final = {'games' : []}
    for game_address in game.grids:
        final['games'].append({
            'address'       :   game_address,
            'width'         :   game.grids[game_address].width,
            'height'        :   game.grids[game_address].height,
            'name'          :   game.grids[game_address].name,
            'description'   :   game.grids[game_address].description
        })
    return jsonify(final)


@app.route('/imgrefresh', methods=['GET'])
def refresh_imgs():
    if game.search():
        for game_address in game.grids:
            pic_path = 'static/img/{}.png'.format(game_address)
            game.gen_grid_image(game_address, pic_path)
    return ''


#This page is meant for manually adding new game addresses and is reserved
#for administrators only, hence the secret-based authentication
@app.route('/add', methods=['POST'])
def add():
    admin_tok = request.form.get('token', None)
    if admin_tok != s.token:
        return 'Failed authentication'
    game_addr = request.form.get('game_address', None)
    start_price = float(request.form.get('start_price', None))
    width = int(request.form.get('width', None))
    height = int(request.form.get('height', None))
    game_name = request.form.get('game_name', None)
    game_desc = request.form.get('game_description', None)
    if all([game_addr, start_price, width, height, game_name, game_desc]):
        game.add_game(game_addr, start_price, width, height, game_name, game_desc)
        return 'Success'
    return 'Failed to add new game'
###
