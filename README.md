# GridPlace Python API

Python web implementation of the [GridPlace blockchain game protocol](https://docs.google.com/document/d/12meO83UyLb10K1QqPUETsgcPVq3SOxhA65HwIFjZK6k/edit?usp=sharing) using the Flask REST API.

## How to play

1. Go to the [website](https://gridplace.duckdns.org/)

2. [Register](https://gridplace.duckdns.org/register) an account

3. Deposit some [Gridcoin](https://gridcoin.us/)

4. [Play](https://gridplace.duckdns.org/games)

## Public API endpoints

The following are a list of API calls followed by their arguments which can be used to play the game on your account.

All calls are GET requests with URL arguments.

To get your user token, simply login on the web and copy the cookie that is generated.

* `place` - places a pixel onto the board

`addr=` - the address of the board you want to place the pixel on

`x=` - the x-coordinate of the pixel you want to place

`y=` - the y-coordinate of the pixel you want to place

`col=` - the colour of the pixel you would like to place. This must be a hex-coded colour value starting with a "#" ("%23" for the URL-safe character)

`token=` - your user token

* `getpixels` - gets the data for all the pixels on a specific board

`addr=` - the address of the board you want the pixels from

* `getgrids` - gets the information about all the currently running games

## Future additions and ideas

* A python script or something to convert a PNG to pixels on the board
