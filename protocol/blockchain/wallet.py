import json
import requests

from . import config as c

def query(cmd, params=[]):
    if not all([isinstance(cmd, str), isinstance(params, list)]):
        raise Exception('Invalid arguments provided')
    command = json.dumps({'method' : cmd, 'params' : params})
    r = requests.post(c.RPC_HOST, auth=(c.RPC_USR, c.RPC_PASS), data=command)
    response = json.loads(r.text)
    if response['error'] != None:
        return 1
    else:
        return response['result']


def send_tx(address, amount, comment=None):
    if amount > 1000:
        print('Warning, amount sent exceeds 1000 GRC:')
        print('Attempted sending {} GRC to {}'.format(amount, address))
        return 1
    args = [address, amount]
    if comment:
        args.append("")
        args.append("")
        args.append(comment)
    return query('sendtoaddress', args)
