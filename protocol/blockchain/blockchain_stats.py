'''
A python library for abstracting Gridcoin RPC commands.
Credentials for RPC login must be written to config.py
(see config.skel.py for a template).

Author: Delta
'''

from . import wallet as w


def get_current_block():
    return w.query('getblockcount', [])


def get_block_data(height):
    results = {}
    block_data = w.query('getblockbynumber', [height])
    results = {
        'height'            :       height,
        'hash'              :       block_data['hash'],
        'time'              :       block_data['time'],
        'difficulty'        :       block_data['difficulty'],
        'nTXs'              :       len(block_data['tx']),
        'tx'                :       block_data['tx'][1:], #indexed to ingore mint transactions
        'mint'              :       block_data['mint'],
        'superblock'        :       block_data['IsSuperBlock'] == 0,
        'staker'            :       block_data['CPID'],
        'staker_address'    :       block_data['GRCAddress'],
        'staker_version'    :       block_data['ClientVersion']
    }
    return results


def get_tx_data(txid):
    tx_data = w.query('gettransaction', [txid])

    #Fetch all input addresses with associated values
    input_addrs = {}
    for vin in tx_data['vin']:
        inp_data = w.query('gettransaction', [vin['txid']])
        txin = inp_data['vout'][vin['vout']]
        input_addrs[txin['scriptPubKey']['addresses'][0]] = txin['value']

    #Filter out the appropriate messages per UTXO
    data_field = tx_data['hashboinc']
    messages = []
    #The string operations here are only compatible with Gridcoin, this will be
    #changed in the future
    for message in data_field.replace('<MESSAGE>', '').split('</MESSAGE>'):
        messages.append(message)
    messages = list(set(messages) - {''})

    #Get unique output addresses with associated values
    vouts = tx_data['vout']
    out_addrs = {}
    for output in vouts:
        if output['scriptPubKey']['type'] in ['pubkeyhash', 'scripthash']:
            out_addrs[output['scriptPubKey']['addresses'][0]] = output['value']

    return {'txid'          :       txid,
            'time'          :       tx_data['time'],
            'senders'       :       input_addrs,
            'receivers'     :       out_addrs,
            'messages'      :       messages}
