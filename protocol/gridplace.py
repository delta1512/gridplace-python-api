'''
A basic python library for handling the GridPlace protocol in an object oriented
manner. The library depends on the following:
- blockchain_stats.py
- PIL image library

Author: Delta
'''


from PIL import Image
from time import time
from math import e
#import blockchain.blockchain_stats as bkch
from . import blockchain as blockchain
bkch = blockchain.blockchain_stats
import re
import struct
import json


def check_game_address(addr):
    if len(addr) != 34:
        return False

    split_addr = addr.split('x')

    #Check the width value
    try:
        int(split_addr[0][2:])
    except:
        return False

    #Check the height value
    try:
        int(split_addr[1])
    except:
        return False

    #Check the initial price value
    try:
        float(split_addr[2].replace('o', '.'))
    except:
        return False

    #Return true if all checks passed
    return True


def decode_game_address(addr):
    split_addr = addr.split('x')
    return {
    'width'     :       int(split_addr[0][2:]),
    'height'    :       int(split_addr[1]),
    'price'     :       float(split_addr[2].replace('o', '.'))
    }


def check_tx_data(data, current_grid):
    split_data = data.split()

    #Check x-position
    try:
        x = int(split_data[0])
        if (0 > x) or (x > current_grid.width):
            return False
    except:
        return False

    #Check y-position
    try:
        y = int(split_data[1])
        if (0 > y) or (y > current_grid.height):
            return False
    except:
        return False

    #Check whether valid colour
    return re.search(r'^#(?:[0-9a-fA-F]{3}){1,2}$', split_data[2])


def decode_tx_data(data):
    split_data = data.split()
    msg = ' '.join(split_data[3:])
    if len(msg) > 100:
        msg = msg[:100]
    return {
    'x'         :        int(split_data[0]),
    'y'         :        int(split_data[1]),
    'col'       :        split_data[2][1:],
    'message'   :        msg
    }


def hex_to_rgb(data):
    return struct.unpack('BBB', bytes.fromhex(data))


def serialise_pixel_array(arr):
    width = len(arr)
    height = len(arr[0])
    for y in range(height):
        for x in range(width):
            yield hex_to_rgb(arr[x][y].colour)


class GridPlace:
    grids = {}
    last_block = 0
    search_lock = False

    def __init__(self, start_block=0, custom_grids=[]):
        self.last_block = start_block
        for grid in custom_grids:
            self.grids[grid.game_address] = grid

    def add_game(self, address, start_price, width, height, name='', description=''):
        self.grids[address] = Grid(address, start_price, width, height, True, name, description)

    def search(self):
        if not self.search_lock:
            #Lock searching
            self.search_lock = True
            new_block = bkch.get_current_block()
            #If there are new blocks on the chain
            if new_block >= self.last_block:
                #For every one of the new blocks
                for block in range(self.last_block, new_block + 1):
                    #For every transaction in each block
                    for txid in (bkch.get_block_data(block))['tx']:
                        tx = bkch.get_tx_data(txid)
                        #print(txid)
                        #For every game address in the outputs of each transaction
                        for i, game_address in enumerate(set(tx['receivers'].keys()) & set(self.grids.keys())):
                            try:
                                self.grids[game_address].edit_pixel(
                                tx['txid'],
                                tx['messages'][0],
                                list(tx['senders'].keys())[0],
                                tx['receivers'][game_address],
                                tx['time']
                                )
                            except Exception as E:
                                #print('pix failed edit', E)
                                pass
                self.last_block = new_block + 1
                #Unlock searching
                self.search_lock = False
                #Return true if new blocks were found
                return True
            #Unlock searching
            self.search_lock = False
            return False
        return False

    def gen_grid_image(self, game_address, file_name):
        if game_address in self.grids:
            self.grids[game_address].render_image(file_name)


class Grid:
    def __init__(self, game_address, start_price=0, width=1, height=1, custom_game=False, name='', description=''):
        self.game_address = game_address
        self.start_price = start_price
        self.width = width
        self.height = height
        self.cells = []
        self.name = name
        self.description = description

        if not custom_game:
            if check_game_address(game_address):
                game_config = decode_game_address(game_address)
                self.width = game_config['width']
                self.height = game_config['height']
                self.start_price = game_config['price']
            else:
                raise Exception('Invalid game address')

        #Load the grid with white pixels
        for x in range(self.width):
            new = [Pixel(self.start_price) for i in range(self.height)]
            self.cells.append(new)

    def edit_pixel(self, txid, tx_data, send_addr, amount, tx_time):
        if not check_tx_data(tx_data, self):
            raise Exception('Invalid transaction data')

        pixel_change = decode_tx_data(tx_data)
        self.cells[pixel_change['x']][pixel_change['y']].edit(
            txid,
            amount,
            pixel_change['col'],
            send_addr,
            pixel_change['message'],
            tx_time
        )

    def render_image(self, file_name):
        out_image = Image.new('RGB', (self.width, self.height))
        out_image.putdata(list(serialise_pixel_array(self.cells)))
        out_image.save(file_name)

    def jsonify(self):
        final = []
        for x in range(self.height):
            new = []
            for y in range(self.width):
                pixel = self.cells[x][y]
                new.append({
                    'colour'    :   hex_to_rgb(pixel.colour),
                    'editor'    :   pixel.last_editor,
                    'time'      :   pixel.last_edit_time,
                    'txid'      :   pixel.last_txid,
                    'price'     :   pixel.get_price()
                })
            final.append(new)
        return {'grid' : final}


class Pixel:
    def __init__(self, init_price, col='ffffff'):
        self.colour = col
        self.init_price = init_price
        self.last_edit_time = 0
        self.last_editor = ''
        self.message = ''
        self.num_edits = 0
        self.last_txid = ''

    def get_price(self):
        return round(self.init_price * (e**(0.125 * self.num_edits)), 8)

    def edit(self, txid, amount_spent, col, editor, msg, tx_time):
        if amount_spent >= self.get_price():
            self.colour = col
            self.last_editor = editor
            self.message = msg
            self.num_edits += 1
            self.last_edit_time = round(tx_time)
            self.last_txid = txid
