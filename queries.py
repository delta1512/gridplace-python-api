from flask import g
import mysql.connector as cnx
from time import time
import qrcode

from protocol import blockchain
w = blockchain.wallet
import crypt
import secret as s


PLACE_TIMEOUT = 2 * 90 #2 confirmations


### String queries
NEW_USER = 'INSERT INTO udb VALUES (%s, %s, %s, %s, %s);'

GET_PWHASH = 'SELECT pwhash FROM udb WHERE uname=%s;'

CHECK_USER_EXISTS = 'SELECT count(uname) FROM udb WHERE uname=%s;'

GET_TOKEN_EXP = 'SELECT expiry FROM tokens WHERE tk=%s;'

GET_ACC_FRM_TK = 'SELECT udb.uname, udb.balance, udb.address FROM udb, tokens WHERE tokens.tk=%s AND tokens.uname=udb.uname'

SET_NEW_TOKEN = 'INSERT INTO tokens VALUES (%s, %s, %s);'

MAKE_TX_OUT = 'INSERT INTO transactions VALUES (%s, %s, %s);'

MAKE_BAL_CHANGES = 'UPDATE udb SET balance = balance + %s WHERE uname=%s;'

GET_BAL = 'SELECT balance FROM udb WHERE uname=%s;'

GET_ALL_USERS = 'SELECT address, uname FROM udb;'

GET_LAST_PLACE = 'SELECT last_place FROM udb WHERE uname=%s;'

SET_LAST_PLACE = 'UPDATE udb SET last_place = %s WHERE uname=%s;'
###


### Basic interface functions
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = cnx.connect(host=s.DB_HOST, user=s.DB_USER, passwd=s.DB_PASS, db=s.DB_NAME)
    return db


def q_ro(query, args=(), one=False):
    cur = get_db().cursor()
    cur.execute(query, args)
    result = cur.fetchall()
    cur.close()
    return (result[0] if result else None) if one else result


def q_wo(query, args=()):
    cur = get_db().cursor()
    cur.execute(query, args)
    cur.close()
    get_db().commit()
###


### Abstract queries
def user_exists(uname):
    return q_ro(CHECK_USER_EXISTS, args=(uname,), one=True)[0] == 1


def valid_token(tk):
    try:
        tk = int(tk)
    except:
        return False
    expiry = q_ro(GET_TOKEN_EXP, args=(tk,), one=True)
    if expiry is None:
        return False
    expiry = q_ro(GET_TOKEN_EXP, args=(tk,), one=True)[0]
    return expiry > time()


def get_account_from_token(tk):
    tk = int(tk)
    account_info = q_ro(GET_ACC_FRM_TK, args=(tk,), one=True)
    final = {}
    final['username'] = account_info[0]
    final['balance'] = account_info[1]
    final['address'] = account_info[2]
    return final


def password_correct(uname, password):
    pwhash = q_ro(GET_PWHASH, args=(uname,), one=True)[0]
    if pwhash is None:
        return False
    return crypt.check_pw(password, bytes(pwhash))


def set_user_token(tk, uname, expiry):
    tk = int(tk)
    q_wo(SET_NEW_TOKEN, args=(tk, uname,  expiry))


def new_user(uname, pwhash):
    address = w.query('getnewaddress')

    qr = qrcode.QRCode(
         version=1,
         error_correction=qrcode.constants.ERROR_CORRECT_L,
         box_size=10,
         border=2)
    qr.add_data(address)
    qr.make(fit=True)
    img = qr.make_image(fill_color='#5c00b3', back_color='white')
    img.save('static/qr/{}.png'.format(uname), format="PNG")

    q_wo(NEW_USER, args=(uname, pwhash, 0, address, 0))


def can_afford(amount, uname):
    return q_ro(GET_BAL, args=(uname,), one=True)[0] > amount


def can_place(uname):
    return q_ro(GET_LAST_PLACE, args=(uname,), one=True)[0]+PLACE_TIMEOUT < time()


def spend(amount, uname, txid):
    q_wo(MAKE_TX_OUT, args=(txid, uname, -amount))
    q_wo(MAKE_BAL_CHANGES, args=(-amount, uname))
    q_wo(SET_LAST_PLACE, args=(int(time()), uname))


def get_addr_uname_dict():
    all_users = q_ro(GET_ALL_USERS)
    final = {}
    for user in all_users:
        final[user[1]] = user[0]
    return final
###
