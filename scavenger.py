from time import sleep
import mysql.connector as cnx

from protocol import blockchain
w = blockchain.wallet
import secret as s


last_block = 0
MIN_DEPOSIT = 0.0125
LAST_BLOCK_FILE = '/tmp/last_block_gridplace'
SLEEP_TIME = 90
db = None


### Basic interface functions
#Copied from queries.py so that circular dependencies are avoided
MAKE_BAL_CHANGES = 'UPDATE udb SET balance = balance + %s WHERE uname=%s;'

GET_ALL_USERS = 'SELECT address, uname FROM udb;'

MAKE_DEPOSIT = 'INSERT INTO transactions VALUES (%s, %s, %s);'

def get_db():
    global db
    if db is None:
        db = cnx.connect(host=s.DB_HOST, user=s.DB_USER, passwd=s.DB_PASS, db=s.DB_NAME)
    return db


def q_ro(query, args=(), one=False):
    cur = get_db().cursor()
    cur.execute(query, args)
    result = cur.fetchall()
    cur.close()
    return (result[0] if result else None) if one else result


def q_wo(query, args=()):
    cur = get_db().cursor()
    cur.execute(query, args)
    cur.close()
    get_db().commit()


def get_addr_uname_dict():
    all_users = q_ro(GET_ALL_USERS)
    final = {}
    for user in all_users:
        final[user[0]] = user[1]
    return final


def register_deposit(txid, amount, uname):
    try:
        q_wo(MAKE_DEPOSIT, args=(txid, uname, amount))
        q_wo(MAKE_BAL_CHANGES, args=(amount, uname))
        return True
    except:
        return False
###


def get_out_addrs_from_tx(tx_data, amounts=False):
    addresses = []
    for output in tx_data['vout']:
        try:
            for addr in output['scriptPubKey']['addresses']:
                addresses.append({addr : output['value']})
        except KeyError:
            addresses.append({'' : 0})
    if amounts:
        return addresses
    return [list(addr.keys())[0] for addr in addresses]


def get_outputs(txid):
    tx = w.query('gettransaction', [txid])
    out_addrs = []
    try:
        if not isinstance(tx, int):
            out_addrs = get_out_addrs_from_tx(tx, True)
    except RuntimeError as E:
        print('Error in get_outputs: {}'.format(E))

    return list(filter(lambda x: x != {'': 0}, out_addrs))


def blk_searcher():
    global last_block
    newblock = w.query('getblockcount', [])
    if newblock > last_block:
        try:
            users = get_addr_uname_dict()
            for blockheight in range(last_block+1, newblock+1):
                last_block = blockheight
                blockdata = w.query('getblockbynumber', [blockheight])
                if isinstance(blockdata, dict):
                    for txid in blockdata['tx']:
                        for received in get_outputs(txid):
                            addr = list(received.keys())[0]
                            uid = users.get(addr, None)
                            if (received[addr] < MIN_DEPOSIT) or (uid is None):
                                continue
                            register_deposit(txid, received[addr], uid)
        except Exception as E:
            print('Exception in blk searcher: {}'.format(E))


with open(LAST_BLOCK_FILE, 'r') as last_block_file:
    last_block = int(last_block_file.read().replace('\n', ''))
while True:
    try:
        with open(LAST_BLOCK_FILE, 'r') as last_block_file:
            last_block = int(last_block_file.read().replace('\n', ''))
        blk_searcher()
        with open(LAST_BLOCK_FILE, 'w') as last_block_file:
            last_block_file.write(str(last_block))
        sleep(SLEEP_TIME)
    except KeyboardInterrupt:
        db.close()
        exit(0)
